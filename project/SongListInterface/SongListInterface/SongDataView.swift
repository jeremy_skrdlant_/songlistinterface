//
//  SongDataView.swift
//  SongListInterface
//
//  Created by Jeremy Skrdlant on 8/15/20.
//  Copyright © 2020 NWKTC. All rights reserved.
//

import SwiftUI

struct SongDataView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct SongDataView_Previews: PreviewProvider {
    static var previews: some View {
        SongDataView()
    }
}
